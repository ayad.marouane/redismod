
    docker-compose up -d

    docker-compose scale sentinel=3

    docker-compose scale slave=4

    connect one of sentinel containers: docker exec -it redis_sentinel_1 bash

    connect redis using redis-cli redis-cli -p 26379

    check master ip SENTINEL get-master-addr-by-name mymaster

    connect master container and add sample data to redis docker exec -it sentinel_master_1 bash redis-cli set xx yy get xx

    connect slave container to read data that is added to master docker exec -it sentinel_slave_1 bash redis-cli get xx

